
program Evalues

  integer, parameter :: dp = kind(1.0d0)
  integer, parameter :: dpc = kind((1.0d0,1.0d0))
  complex(dpc), parameter :: zzero = cmplx(0.d0, 0.d0)

  integer :: nstate, alcstat, k  
  complex(dpc), allocatable, dimension(:) :: eval


  print *, 'Nstate? '
  read *, nstate

! Allocate memory for eigenvalues of (H + i Gamma)
  allocate(eval(nstate), stat=alcstat) 
!  call alccheck('eval', nstate, alcstat)


  eval(:) = zzero


    open(88, file='Eval.dat', form='unformatted', status='old')
    read(88) eval
    close(88) 




	open(1001,file='Eval.txt',form='formatted')

      EnergyLoop: do k=1, nstate
         

	   ! Write Transmission TE to disk
           write(1001,*) eval(k)
           call flush(1001)
!	   close(1001)

      enddo EnergyLoop
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! End loop over energy
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	close(1001)

      deallocate(eval)


end program Evalues


