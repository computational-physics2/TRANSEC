#! /home/sfzhu/anaconda3/bin/python

# Script by Shifeng Zhu, May 2021
# This script is to import the TE dat and export them to the CSV file.
# python3 "TE.TOT*dat" csv_filename
import glob, sys, csv

# This part is to sort the file names based on their index, not by alphabetical order.
csv_filename = "TE_data.csv"
if(len(sys.argv) >= 3):
    csv_filename = sys.argv[2] + ".csv"

orig_files  = glob.glob(sys.argv[1])

if (not orig_files):
    print("\nFile not found")
    print("Usage: <python> AvgTE.py 'fileID', where fileID is a string (note the single quotes) representing all T(E) files.  fileID can include a path and/or a wildcard pattern.\n")
    exit()

orig_files_index = [int(cur_str[3:])  for cur_filename in orig_files for cur_str in cur_filename.split('.') if len(cur_str) > 3 and cur_str[3:].isdigit()]
if ( len(orig_files_index) != len(orig_files)):
    print("\nFormat issue, it should have the format 'Kpt#' in the file name.")
    exit()

files = [x for _,x in sorted(zip(orig_files_index, orig_files))]
file_index = sorted(orig_files_index) 

numEPts = -2
TE_field_names = ['E-Ef/eV']
TE_data = []
with open(files[0], 'r') as fObj:
    for line in fObj:
        if (numEPts >= 0):
            TE_data.append([float(line.split()[1])])
        numEPts += 1

for i, f in enumerate(files):
    TE_field_names.append("Kpt " + str(file_index[i]))
    n = -2
    with open(f, 'r') as fObj:
        for line in fObj:
            if(n >= 0):
                TE_data[n].append(float(line.split()[2]))
            n += 1

with open(csv_filename, 'w') as f:
    write = csv.writer(f)
    
    write.writerow(TE_field_names)
    write.writerows(TE_data)
