
program Evectors

  integer, parameter :: dp = kind(1.0d0)
  integer, parameter :: dpc = kind((1.0d0,1.0d0))
  complex(dpc), parameter :: zzero = cmplx(0.d0, 0.d0)

  integer :: nstate, nstateStart, nstateEnd, ngrid, alcstat, k  
  complex(dpc), allocatable, dimension(:) :: evec


  print *, 'Ngrid? '
  read *, ngrid
  print *, 'Nstate? '
  read *, nstate
  print *, 'Nstate Start? '
  read *, nstateStart
  print *, 'Nstate End? '
  read *, nstateEnd

! Allocate memory for eigenvalues of (H + i Gamma)
  allocate(evec(ngrid), stat=alcstat) 
!  call alccheck('eval', nstate, alcstat)


  evec(:) = zzero


    open(88, file='Evec.dat', form='unformatted', status='old')
	open(1001,file='Evec.txt',form='formatted')

      StateLoop: do k=1, nstate
           read(88) evec
		   if (k >= nstateStart .and. k <= nstateEnd) then
				write(1001,*) 'Evec ', k, ': '
				write(1001,*) evec  
				call flush(1001)  
		   endif 
      enddo StateLoop
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! End loop over states
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	close(1001)
    close(88) 

      deallocate(evec)


end program Evectors


