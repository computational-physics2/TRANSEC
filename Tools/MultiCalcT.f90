
program MultiCalcT

  use omp_lib
  
  integer, parameter :: dp = kind(1.0d0)
  integer, parameter :: dpc = kind((1.0d0,1.0d0))
  complex(dpc), parameter :: zzero = cmplx(0.d0, 0.d0), zone = cmplx(1.d0, 0.d0), zi = cmplx(0.d0, 1.d0)
  real(dp), parameter :: tol = 5.d-6

  integer :: ndim, nstateT, alcstat, i, j, k
  integer :: myid, ierr, nprocs 
  integer, allocatable, dimension(:) :: nstate
  complex(dpc), dimension (:,:), pointer ::  GamR, GamL  
  complex(dpc), allocatable, dimension(:) :: eval, r, w
  complex(dpc), allocatable, dimension(:,:) :: evec, evecNoRz
  real(dp), allocatable, dimension(:,:) :: CAP
  complex(dpc) :: tau
  logical :: KPtNoRz 
  !real(dp) :: Emin, Emax, dE, E
  character(len=150) :: evecFile, evecNoRzFile, evalFile, CAPFile, d, t
  
  call date_and_time(d,t)
  open(25, file='MultiCalcT.out', form='formatted', status='replace')
  write(25,*) d, t
  

  print *, 'NDim? '
  read *, ndim
  
  print *, '2-D periodic system lacking R_z,pi:(x,y,z)->(-x,-y,z) symmetry? '
  read *, KPtNoRz

  print *, 'Number of Intervals? '
  if (KPtNoRz) print *, '(Note: Double this number of eigenvector files will be needed to account for both +k and -k points)'
  read *, nfiles
  allocate(nstate(nfiles), stat=alcstat)
  
  nstateT = 0
  do i = 1, nfiles
	print *, 'Nstate in file # ', i, '? '
	read *, nstate(i)
	nstateT = nstateT + nstate(i)
  enddo
  
  write(25,*) 'NDim = ', ndim
  write(25,*) nfiles, ' sets of files to be read, total of ', nstateT, 'vectors'
  
! Allocate memory for eigenvalues & vectors of (H + i Gamma), and for left and right CAPs
  allocate(eval(nstateT), stat=alcstat) 
!  call alccheck('eval', nstateT, alcstat)  
  allocate(evec(ndim, nstateT), stat=alcstat)
  if (KPtNoRz) allocate(evecNoRz(ndim, nstateT), stat=alcstat)
  allocate(CAP(ndim, 2), stat=alcstat)

  evec(:,:) = zzero
  CAP(:,:) = zzero
  eval(:) = zzero

  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read in nfiles sets of files
  k = 1
  do i = 1, nfiles
	print *, 'Evec file #', i, '? '
	if (KPtNoRz) print *, ' +k point: '
	read *, evecFile
	if (KPtNoRz) then
		print *, ' -k point: '
		read *, evecNoRzFile
	endif 
	print *, 'Eval file #', i, '? '
	read *, evalFile
	print *, 'CAP file #', i, '? '
	read *, CAPFile
	! Read in evectors, evalues, and CAPs from first set of files
	open(88, file=evalFile, form='unformatted', status='old')
    read(88) eval(k:(k+nstate(i)-1))
    close(88) 
	if (i == 1) then
		open(88, file=CAPFile, form='unformatted', status='old')
		read(88) CAP(:,1)
		read(88) CAP(:,2)
		close(88)
	endif 
    open(88, file=evecFile, form='unformatted', status='old')
    do j = 1, nstate(i)
		read(88) evec(:,k+j-1)
	enddo
    close(88)
	if (KPtNoRz) then
	    open(88, file=evecNoRzFile, form='unformatted', status='old')
		do j = 1, nstate(i)
			read(88) evecNoRz(:,k+j-1)
		enddo
		close(88)
	endif
	k = k + nstate(i)
	write(25,*) 'Read file ', evecFile, evalFile
   enddo


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Check whether Gram-Schmidt is needed to orthogonalize eigenvectors 
! in same eigenspace (based on q^t dot q').  
! Also eliminate duplicates, and normalize eigenvectors based on q^t dot q = 1.  
  i = nstate(1)+1
  do k = 1, nstateT - nstate(1)   ! NOTE: nstateT can be changed by this loop, but Fortran should still loop up to original value (copy) of nstateT
     print *, k
	 write(25,*) k
	 ! Check whether in same eigenspace as previous vectors
     do j = i-1, 1, -1
     	!if ( abs(eval(i)-eval(j)) > tol )  exit
		if ( abs(eval(i)-eval(j)) <= tol )  then
			! Orthogonalize (based on q^t dot q')
			tau = dot_product(conjg(evec(:,j)), evec(:,i))
			evec(:,i) = evec(:,i) - tau * evec(:,j)
			
			if (KPtNoRz) then
				tau = dot_product(conjg(evecNoRz(:,j)), evecNoRz(:,i))
				evecNoRz(:,i) = evecNoRz(:,i) - tau * evecNoRz(:,j)
			endif 

		endif 
     enddo

     ! Eliminate duplicates, and normalize (based on q^t dot q = 1)
     tau = dot_product(conjg(evec(:,i)), evec(:,i))
     if (abs(tau) > tol) then
		print *, 'Non-duplicate', tau
		write(25,*), 'Non-duplicate', tau
		evec(:,i) = evec(:,i) / sqrt(tau)
		i = i+1
	 else
		print *, 'Duplicate: ', i, eval(i), tau
		write(25,*), 'Duplicate: ', i, eval(i), tau
		!nstate2 = nstate2 - 1
		nstateT = nstateT - 1
		evec(:, i:nstateT) = evec(:, (i+1):(nstateT+1))
		eval(i:nstateT) = eval((i+1):(nstateT+1))
	 endif 
	 
	 if (KPtNoRz) then
		tau = dot_product(conjg(evecNoRz(:,i)), evecNoRz(:,i))
		if (abs(tau) > tol) then
			print *, '-k:  Non-duplicate', tau
			write(25,*), '-k:  Non-duplicate', tau
			evecNoRz(:,i) = evecNoRz(:,i) / sqrt(tau)
			i = i+1
		else
			print *, '-k:  Duplicate: ', i, eval(i), tau
			write(25,*), '-k:  Duplicate: ', i, eval(i), tau
			!nstate2 = nstate2 - 1
			!nstateT = nstateT - 1
			evecNoRz(:, i:nstateT) = evecNoRz(:, (i+1):(nstateT+1))
			!eval(i:nstateT) = eval((i+1):(nstateT+1))
		endif 

	 endif
  enddo 

  write(25,*) 'Finished vectors'

  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Compute CAPs GamL, GamR in diagonal basis of (H + i Gamma)
  allocate(GamL(nstateT, nstateT), stat=alcstat)
  allocate(GamR(nstateT, nstateT), stat=alcstat)
  allocate(r(nstateT), stat=alcstat) 
  allocate(w(nstateT), stat=alcstat) 
  GamL(:,:) = zzero
  GamR(:,:) = zzero

  !$OMP parallel do private (i, j, k, tau, r, w) collapse (1)
  do i = 1, nstateT
	 !r(:) = zzero
	 !w(:) = zzero
     do j = 1, i
		if (modulo(j,500) == 0) write(25,*) 'thread ', omp_get_thread_num(),': GammaTilde row ', i, 'col', j, 'of', nstateT
		r(j) = zzero
		w(j) = zzero
		do k = 1, ndim
			tau = conjg(evec(k, i)) * evec(k, j) 

	   !!!!! If no R_z,pi symm, add explicit matrix elements for -k points
	   if (KPtNoRz) then  
	   		!if (bestMatchInd(igrid) < 0  .or.  bestMatchInd(jgrid) < 0) then
	   		!	tau = zzero
	   		!else
!				tau = (tau * (zone + zi) + (conjg(evecNoRz(kgrid, bestMatchInd(igrid))) * &
!					evecNoRz(kgrid, bestMatchInd(jgrid)) * (zone - zi) )) / cmplx(two, zero)
				tau = (tau * (zone + zi) + (conjg(evecNoRz(k, i)) * &
					evecNoRz(k, j) * (zone - zi) )) / cmplx(2.d0, zero)
			!endif 
	   endif 

			!GamL(i, j) = GamL(i, j) + tau * cmplx( CAP(k, 1), 0.d0)
			!GamR(i, j) = GamR(i, j) + conjg(tau) * cmplx( CAP(k, 2), 0.d0)
			r(j) = r(j) + tau * cmplx( CAP(k,1), 0.d0)
			w(j) = w(j) + tau * cmplx( CAP(k,2), 0.d0)
		enddo
	 
     GamL(i, j) = r(j)
     GamR(j, i) = w(j)
	 enddo
  enddo
  !$OMP end parallel do

  ! Explicitly set the full transformed CAPs using their Hermiticity
  !$OMP parallel do private (i, j, k, tau, r, w) collapse (1)
  do i = 1, nstateT
    GamL(1:(i-1), i) = conjg(GamL(i, 1:(i-1)))
    GamL(i, i) = real(GamL(i, i))
    GamR(i, 1:(i-1)) = conjg(GamR(1:(i-1), i))
    GamR(i, i) = real(GamR(i, i))
  enddo
  !$OMP end parallel do
  
! Deallocate eigenvectors of (H + i Gamma) -- no longer needed
  deallocate(evec)

! Write Eval, GamL, GamR to disk
  open(88, file='Eval.dat', form='unformatted', status='replace')
  write(88) eval(1:nstateT)
  close(88)
  open(88, file='GammaL.dat', form='unformatted', status='replace')
  open(89, file='GammaR.dat', form='unformatted', status='replace')
  write(88) GamL(1:nstateT,1:nstateT)
  write(89) GamR(1:nstateT,1:nstateT)
  close(88)
  close(89)


  print *
  print *
  print *, 'Nstate = ', nstateT
  do i = 1, nstateT
	print *, i, eval(i)
  enddo

  write(25,*)
  write(25,*) 
  write(25,*), 'Nstate = ', nstateT
  do i = 1, nstateT
	write(25,*), i, eval(i)
  enddo
  write(25,*)
  write(25,*) 
  
  call date_and_time(d,t)
  write(25,*) ' Done ', d, t
  close(25)
  
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Deallocate memory  
      if (associated (GamL)) deallocate(GamL)
	  if (associated (GamR)) deallocate(GamR)
      if (allocated (evec)) deallocate(evec)
	  if (allocated (evecNoRz)) deallocate(evecNoRz)  
      if (allocated (CAP)) deallocate(CAP)
      if (allocated (eval)) deallocate(eval)
	  if (allocated (r)) deallocate(r)
	  if (allocated (w)) deallocate(w)	  
	  if (allocated (nstate)) deallocate(nstate)
	  
end program MultiCalcT
