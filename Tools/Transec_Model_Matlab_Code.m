
% Setup Hermitian H and gaussian CAPs.  

dim = 20; 

H = diag(-6*ones(dim-1,1), 1);  
H(dim/2-1,dim/2)=-2;
H(dim/2,dim/2+1)=-2;
H = H + H';  
%H = rand(dim);


x = (1:dim) ;
gammaL = 3*exp(-(x-1).^2 / 18 );
gammaR = 3*exp(-(dim-x).^2 / 18 );

 
A = H - i*diag(gammaL+gammaR,0);



% Do exact diagonalization of H - i Gamma, use this to compute T(E)

tic; [U,epsilon] = eig(A); toc
epsilon = diag(epsilon);
% H - i Gamma is complex orthogonal, so the eigenvectors should be
% normalized according to the (non-positive-definite) inner product
for (j = 1:dim)  
    for (k = 1:(j-1))
        if (abs(epsilon(k) - epsilon(j)) < 1e-6)
           U(:,j) = U(:,j) - ( transpose(U(:,k))*U(:,j) )* U(:,k);
        end
    end
    U(:,j) = U(:,j) / sqrt( transpose(U(:,j)) * U(:,j) );
end

gammaLTild = U' * diag(gammaL,0) * U;
gammaRTild = transpose(U) * diag(gammaR,0) * conj(U);


NEpts = 200;
E = -16 + (1:NEpts) * 32 / NEpts; 
Tfull = zeros(NEpts, 1);

for (n = 1:NEpts)
	for (j=1:dim)
		for (k=1:dim)

			Tfull(n) = Tfull(n) + 4*gammaLTild(j,k)*gammaRTild(k,j) / ((E(n) - epsilon(k)) * (E(n) - conj(epsilon(j))));

		end
	end
end



plot(E,real(Tfull),'-o', E, imag(Tfull)) 
legend('Re{T(E)}', 'Im{T(E)}') 

