#! /usr/bin/env python

# Script by Baruch Feldman, February 2017 
# This script is part of TRANSEC, the real-space electronic transport module based on the PARSEC DFT code
# Useful for averaging the T(E) curve over k-points and/or spins  
# Usage: <python> AvgTE.py 'fileID', where fileID is a string (note the single quotes) representing all T(E) files to average.  fileID can include a path and/or a wildcard pattern   
# Averages over T(E) files specified in fileID either explicitly or using a wildcard pattern  


import glob, sys

files = glob.glob(sys.argv[1])
if (not files):
	print "\nFile not found"
	print "Usage: <python> AvgTE.py 'fileID', where fileID is a string (note the single quotes) representing all T(E) files to average.  fileID can include a path and/or a wildcard pattern.\n" 
	exit()


# Count, and keep a copy of, the energies from the first file matching fileID    
numEPts = -2  # Account for the two header lines in TE.dat 
E = []
RelE = []
sumT = []
with open(files[0], 'r') as fObj:
	for line in fObj:
		if (numEPts >= 0): 
			E.append(float(line.split()[0]))
			RelE.append(float(line.split()[1]))
			sumT.append(0)
		numEPts += 1
	
nFiles = 0

# Average T(E) over all files matching fileID  
for f in files:
	n = -2  # Account for the two header lines in TE.dat 
	nFiles += 1
	
	with open(f, 'r') as fObj:
		for line in fObj:
			if (n >= 0):  # Account for the two header lines in TE.dat 
				sumT[n] += float(line.split()[2])
			n += 1

# Output results, including energies
print "Averaged over " + str(nFiles) + " T(E) files\n" 
print "E (Ry) \t E - E_F (eV) \t < T(E) >" 
for n in range(numEPts):
	print E[n], '\t', RelE[n], '\t', sumT[n] / nFiles 



