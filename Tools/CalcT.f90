
program CalcT

  integer, parameter :: dp = kind(1.0d0)
  integer, parameter :: dpc = kind((1.0d0,1.0d0))
  complex(dpc), parameter :: zzero = cmplx(0.d0, 0.d0)

  integer :: NE 

  integer :: nstate, alcstat, i, j, k, nstateUse
  complex(dpc), dimension (:,:), pointer ::  GamLR  
  complex(dpc), allocatable, dimension(:) :: eval
  complex(dpc) TE
  real(dp) :: Emin, Emax, dE, E


  print *, 'Nstate? '
  read *, nstate
  print *, 'Emin? '
  read *, Emin
  print *, 'Emax? '
  read *, Emax
  print *, 'NE? '
  read *, NE
  print *, 'Nstate Use?'
  read *, nstateUse

  dE = (Emax - Emin) / (dble(NE) - 1.0d0)

! Allocate memory for CAP product GamLR_ij = (GamL_ij * GamR_ij) in diagonal basis of (H + i Gamma)
  allocate(GamLR(nstate,nstate), stat=alcstat) 
!  call alccheck('gamlr', nstate**2, alcstat)

! Allocate memory for eigenvalues of (H + i Gamma)
  allocate(eval(nstate), stat=alcstat) 
!  call alccheck('eval', nstate, alcstat)


  GamLR(:,:) = zzero
  eval(:) = zzero


    open(88, file='GammaLR.dat', form='unformatted', status='old')
    read(88) GamLR
    close(88)

    open(88, file='Eval.dat', form='unformatted', status='old')
    read(88) eval
    close(88) 




	open(1001,file='TE.dat',form='formatted')

 E = Emin

      EnergyLoop: do k=1, NE
         

	 ! Set T(E) = 0, to be summed 
         TE = zzero


           do i=1, nstateUse
             do j=1, nstateUse

               TE = TE + GamLR(i, j) / &
	       	  ((cmplx(E,0.d0) - eval(j)) * (cmplx(E,0.d0) - conjg(eval(i))))

             enddo
           enddo
           

 	   TE = TE * cmplx(4.d0, 0.d0) 

	   ! Write Transmission TE to disk
           write(1001,*) E, real(TE), aimag(TE)
           call flush(1001)
!	   close(1001)

           E = E + dE

      enddo EnergyLoop
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! End loop over energy
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	close(1001)


      if (associated (GamLR)) deallocate(GamLR)
      deallocate(eval)


end program CalcT
