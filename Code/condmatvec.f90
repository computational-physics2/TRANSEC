!==============================================================================!
!     Have EI-A, where E is an energy, I is the unit matrix, and A is          !
!     The Hamiltonian including complex absorbing potentials, act on a vector. !
!==============================================================================!
! Copyright (C) 2011-2022 Baruch Feldman, based on Parsec with modifications for Transec

      subroutine zcondmatvec(kplp,gammafull,Energy,xin,xout,RetAdv,parallel) 

! zcondmatvec() calls zmatvec(), the complex version of the matrix-vector application 
! routine in PARSEC in order to include k points, and specifically the anti-Hermitian 
! part ik*del of the k-restricted Hamiltonian H_k 

! NOTE: this subroutine needs no parallelization. "gammafull", "xin", "xout" 
! etc already contain the relevant slice of each processor and all 
! parallelized operations are performed within subroutines invoked
! by matvecB.
      
      use constants
      use parallel_data_module
      use conductance_module
      use electronic_struct_module
      use grid_module
      use matvecB_interface
      
      implicit none
!     Input/Output variables:
!     The parallel environment
      type(parallel_data), intent(in) :: parallel
!     1 - Use retarded Green's function, -1 - use advanced Green's function
      integer, intent(in) :: RetAdv
!     Imaginary part of Hamiltonian      
      real(dp), intent(in) :: gammafull(parallel%ldn)
!	Current k-point 
	integer, intent(in) ::  kplp
!     Energy at which the transmission cofficient is to be calculated      
      real(dp), intent(in) :: Energy
!     The vector to multiply
      complex(dpc), intent(in) :: xin(parallel%ldn)
!     The output
      complex(dpc), intent(out) :: xout(parallel%ldn)
!     Work arrays:
      real(dp), dimension(parallel%ldn) :: xinr, xini, xoutr, xouti
      integer :: i

      !external zmatvecB 

!     Calculate H_KS*xin
      call zmatvecB(kplp,1,xin,xout,parallel%ldn)
      
!     Below we multiply (EI-H_KS-i*Gamma)x.
!     RetAdv changes the sign of the imaginary part of the Hamiltonian,
!     i*Gamma, according to whether we are performing an advanced 
!     (G^a)^{-1}*Gamma_a or retarded (G^r)^{-1}*Gamma_r calculation
 
      do i=1, parallel%mydim
         xout(i) = Energy*xin(i) - xout(i) - zi*RetAdv*gammafull(i)*xin(i)
      enddo
      
      end subroutine zcondmatvec


      subroutine condmatvec(kplp,gammafull,Energy,xin,xout,RetAdv,parallel) 

! NOTE: this subroutine needs no parallelization. "gammafull", "xin", "xout" 
! etc already contain the relevant slice of each processor and all 
! parallelized operations are performed within subroutines invoked
! by matvecB.
      
      use constants
      use parallel_data_module
      use conductance_module
      use electronic_struct_module
      use grid_module
      use matvecB_interface 
      
      implicit none
!     Input/Output variables:
!     The parallel environment
      type(parallel_data), intent(in) :: parallel
!     1 - Use retarded Green's function, -1 - use advanced Green's function
      integer, intent(in) :: RetAdv
!     Imaginary part of Hamiltonian      
      real(dp), intent(in) :: gammafull(parallel%ldn)
!	Current k-point 
	integer, intent(in) ::  kplp
!     Energy at which the transmission cofficient is to be calculated      
      real(dp), intent(in) :: Energy
!     The vector to multiply
      complex(dpc), intent(in) :: xin(parallel%ldn)
!     The output
      complex(dpc), intent(out) :: xout(parallel%ldn)
!     Work arrays:
      real(dp), dimension(parallel%ldn) :: xinr, xini, xoutr, xouti
      integer :: i

      !external matvecB

      do i=1,parallel%mydim
         xinr(i) = real(xin(i))
         xini(i) = aimag(xin(i))
      enddo
      
!     Calculate H^r*x^r
      call matvecB(kplp,1,xinr,xoutr,parallel%ldn)
!     Calculate H^r*x^i
      call matvecB(kplp,1,xini,xouti,parallel%ldn)
      
!     Below we multiply (EI-H)x = (EI - (H^r + iH^i))*(x^r + ix^i)
!     this gives (EI-H)x = [EIx^r - H^r*x^r + H^i*x^i] +
!     + i[EIx^i - H^r*x^i - H^i*x^r].
!     RetAdv changes the sign of the imaginary part of the Hamiltonian,
!     H^i, according to whether we are performing an advanced 
!     (G^a)^{-1}*Gamma_a or retarded (G^r)^{-1}*Gamma_r calculation
 
      do i=1, parallel%mydim
         xoutr(i) = Energy*xinr(i) - xoutr(i) + RetAdv*gammafull(i)*xini(i)
         xouti(i) = Energy*xini(i) - xouti(i) - RetAdv*gammafull(i)*xinr(i)
         xout(i)  = cmplx(xoutr(i),xouti(i))
      enddo
      
      end subroutine condmatvec
