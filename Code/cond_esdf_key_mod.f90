!===============================================================
!
! Copyright (C) 2005 Finite Difference Research Group
! This file is part of parsec, http:==www.ices.utexas.edu=parsec=
! And copyright (C) 2011-2022 Baruch Feldman, with modifications for Transec 
!
! Module to hold keyword list. this must be updated as
! new keywords are brought into existence.
!
! The 'label' is the label as used in calling the esdf routines
! 'typ' defines the type, with the following syntax. it is 3
! characters long.
! the first indicates:
!      i - integer
!      s - single
!      d - double
!      p - physical
!      t - string (text)
!      e - defined (exists)
!      l - boolean (logical)
!      b - block
! the second is always a colon (:)
! the third indicates the "level" of the keyword
!      b - basic
!      i - intermediate
!      e - expert
!      d - dummy
!
! 'Dscrpt' is a description of the variable. it should contain a
! (short) title enclosed between *! ... !*, and then a more detailed
! description of the variable.
!
!---------------------------------------------------------------
module cond_esdf_key

use esdf_key

  implicit none 
  
  contains 
  subroutine init_cond_esdf_key
  !COND
  
  integer, parameter :: PARSEC_KYWDS = 146 
  
  kw_label(PARSEC_KYWDS + 1) = 'gamma_pot' 
  kw_typ(PARSEC_KYWDS + 1) = 'B:E' 
  kw_dscrpt(PARSEC_KYWDS + 1)  = '*! absorbing potential for conduction calcs !*' 
  
  kw_label(PARSEC_KYWDS + 2)   = 'cond_temp' 
  kw_typ(PARSEC_KYWDS + 2)     = 'D:B' 
  kw_dscrpt(PARSEC_KYWDS + 2)  = '*!electronic temperature for conduction calcs !*' 
  
  kw_label(PARSEC_KYWDS + 3)   = 'cond_bias' 
  kw_typ(PARSEC_KYWDS + 3)     = 'D:B' 
  kw_dscrpt(PARSEC_KYWDS + 3)  = '*!external bias for conduction calcs !*' 
  
  kw_label(PARSEC_KYWDS + 4)   = 'cond_fermcut' 
  kw_typ(PARSEC_KYWDS + 4)     = 'D:B' 
  kw_dscrpt(PARSEC_KYWDS + 4)  = '*!Fermi-Dirac threshold temperature for conduction calcs !*' 
  
  kw_label(PARSEC_KYWDS + 5)   = 'cond_ne' 
  kw_typ(PARSEC_KYWDS + 5)     = 'I:B' 
  kw_dscrpt(PARSEC_KYWDS + 5)  = '*!Number of energy point in the Transmittance spectrum !*' 

  kw_label(PARSEC_KYWDS + 6)   = 'cond_tol' 
  kw_typ(PARSEC_KYWDS + 6)     = 'D:B' 
  kw_dscrpt(PARSEC_KYWDS + 6)  = '*!Tolerance for convergence of CSYM in NEGF conductance calcs !*' 

  kw_label(PARSEC_KYWDS + 7)   = 'cond_restart' 
  kw_typ(PARSEC_KYWDS + 7)     = 'I:B' 
  kw_dscrpt(PARSEC_KYWDS + 7)  = '*! Continue NEGF conductance calc from previous run?   !*' 
  
  kw_label(PARSEC_KYWDS + 8)   = 'cond_shift' 
  kw_typ(PARSEC_KYWDS + 8)     = 'D:B' 
  kw_dscrpt(PARSEC_KYWDS + 8)  = '*! Real shift in Hamiltonian for conduction calculations  !*' 

  kw_label(PARSEC_KYWDS + 9)   = 'cond_epairs' 
  kw_typ(PARSEC_KYWDS + 9)     = 'I:B' 
  kw_dscrpt(PARSEC_KYWDS + 9)  = '*! Number of eigen- (value + vector) pairs to use in NEGF conductance  !*' 
  
  kw_label(PARSEC_KYWDS + 10)   = 'cond_prop_epairs' 
  kw_typ(PARSEC_KYWDS + 10)     = 'D:B' 
  kw_dscrpt(PARSEC_KYWDS + 10)  = '*! Proportion of total eigen- (value + vector) pairs to use in NEGF conductance  !*' 

  kw_label(PARSEC_KYWDS + 11)   = 'cond_write_evec' 
  kw_typ(PARSEC_KYWDS + 11)     = 'L:B' 
  kw_dscrpt(PARSEC_KYWDS + 11)  = '*! Write Eigenvectors (large checkpoint / continuation files) in conduction calculations?  !*' 

  kw_label(PARSEC_KYWDS + 12)   = 'cond_use_inv_symm' 
  kw_typ(PARSEC_KYWDS + 12)     = 'L:I' 
  kw_dscrpt(PARSEC_KYWDS + 12)  = &
	'*! Use 2-D Inversion symmetry (R_z,pi: (x,y,z) -> (-x, -y, +z)) in conduction calculations with 2-D PBCs?  !*' 

  kw_label(PARSEC_KYWDS + 13)   = 'cond_detect_symm' 
  kw_typ(PARSEC_KYWDS + 13)     = 'L:E' 
  kw_dscrpt(PARSEC_KYWDS + 13)  = '*! Automatically detect symmetry in conduction calculations with 2-D PBCs?  !*' 

  kw_label(PARSEC_KYWDS + 14)   = 'cond_skip_kpoints' 
  kw_typ(PARSEC_KYWDS + 14)     =  'B:E' 
  kw_dscrpt(PARSEC_KYWDS + 14)  =  &
	'*! List of k-points (referenced by index) to ignore (i.e., skip calculation) in the conduction calculation !*' 

  kw_label(PARSEC_KYWDS + 15)   = 'cond_mpi_groups' 
  kw_typ(PARSEC_KYWDS + 15)     = 'I:E' 
  kw_dscrpt(PARSEC_KYWDS + 15)  = '*! Number of MPI groups of PEs in conduction calculations !*' 
  
  kw_label(PARSEC_KYWDS + 16)   = 'cond_parallelize_kpoint_sign' 
  kw_typ(PARSEC_KYWDS + 16)     = 'L:E' 
  kw_dscrpt(PARSEC_KYWDS + 16)  = '*! Parallelize over the sign of k-points when R_z,pi symmetry is absent? !*' 

  end subroutine init_cond_esdf_key

  !END COND
  
end module cond_esdf_key
!===============================================================
