Updated 7/2022
6/2014
Baruch Feldman

The ./HChain/ is a lead-device-lead configuration, with the electrodes being monatomic chains of Hydrogen atoms with spacing 2 Bohr, and a central "device" Hydrogen atom separated by 4 Bohr from the electrodes.  As expected analytically, T(E) is an approximately Lorentzian peak of height T(E~E_F) = 1 (the number of transmission modes around E ~ E_F in the device atom) and width that depends on the electrode-device gap.  This is the same simulation as in ./Fig2a_LeadHLead/, except that it has been updated with a newer version of TRANSEC.  I highly recommend using this calculation as a quick "sanity test" whenever you install or make changes to the TRANSEC code.  

For the atomic chain test results in ./ , the results have been compared (see figure 2 of the TRANSEC paper) to runs with the electronic transport code, TIMES [Sharma et al., J. Appl. Phys. 113, 203708 (2013); arXiv:1302.1041].  For these TIMES runs, I noticed that TIMES.out reported non-unitary S-matrices (T + R differing from the # of channels) in many cases.  TIMES is sensitive, and this sometimes happens when the basis set is big, the number of atoms in the electrode is bigger than minimal, or the tolerances aren’t tuned.  This can sometimes be a sign of a big numerical problem, but in the present cases I don’t think it’s major because:
	a) For most energy points, T+R was close to the # of channels (nearly unitary) although there were a few exceptions
	b) The calculations agreed at least qualitatively with some other ones (e.g. with smaller basis sets) that were unitary (or very nearly unitary)
	c) They also agreed with our analytical expectations
	d) Generally, they were reciprocal, i.e. agreed in both directions across the system (STest.dat and SProperty.dat agree), which is a good sign


For ./Fig2c_Lead3HLead/, I apparently didn't keep all the input and output from OpenMX + TIMES (including TIMES.out, the Hamiltonian & overlap matrices from OpenMX, etc.).  However, included here are input files that should be enough to run OpenMX, get the *.scfout file which contains the Hamiltonian and overlap matrices, and then put this information into TIMES and generate the included SProperty.dat file.  


For all the OpenMX calculations, I included a copy of the pseudopotentials used, because these are somewhat old and have been replaced by newer ones on the OpenMX website.  (For the same reason, I changed DATA.PATH to ./ in the OpenMX input files.  But you may have to creat VPS and PAO subdirectories and put the pseudo and orbital files there, respectively, because OpenMX typically looks for these as subdirectories of DATA.PATH).  



